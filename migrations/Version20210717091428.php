<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210717091428 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE timeline (id INT AUTO_INCREMENT NOT NULL, station_id INT NOT NULL, portable_equipment_id INT NOT NULL, date DATE NOT NULL, booked INT NOT NULL, available INT NOT NULL, INDEX IDX_46FEC66621BDB235 (station_id), INDEX IDX_46FEC666B5AF2FC4 (portable_equipment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE timeline ADD CONSTRAINT FK_46FEC66621BDB235 FOREIGN KEY (station_id) REFERENCES station (id)');
        $this->addSql('ALTER TABLE timeline ADD CONSTRAINT FK_46FEC666B5AF2FC4 FOREIGN KEY (portable_equipment_id) REFERENCES portable_equipment (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE timeline');
    }
}
