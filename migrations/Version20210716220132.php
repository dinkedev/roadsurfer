<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210716220132 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Initial DB Model';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE  `order`  (id INT AUTO_INCREMENT NOT NULL, campervan_id INT NOT NULL, start_station_id INT NOT NULL, end_station_id INT NOT NULL, start_date DATE NOT NULL, end_date DATE NOT NULL, INDEX IDX_E6AB879DB9D53E94 (campervan_id), INDEX IDX_E6AB879D53721DCB (start_station_id), INDEX IDX_E6AB879D2FF5EABB (end_station_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE campervan (id INT AUTO_INCREMENT NOT NULL, station_id INT NOT NULL, model VARCHAR(255) NOT NULL, is_rented TINYINT(1) NOT NULL, INDEX IDX_6891BB7F21BDB235 (station_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ordered_equipment (id INT AUTO_INCREMENT NOT NULL, parent_order_id INT NOT NULL, portable_equipment_id INT NOT NULL, quantity INT NOT NULL, INDEX IDX_1308E95D1252C1E9 (parent_order_id), INDEX IDX_1308E95DB5AF2FC4 (portable_equipment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE portable_equipment (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE portable_equipment_station (id INT AUTO_INCREMENT NOT NULL, portable_equipment_id INT NOT NULL, station_id INT NOT NULL, quantity INT NOT NULL, INDEX IDX_E66E257BB5AF2FC4 (portable_equipment_id), INDEX IDX_E66E257B21BDB235 (station_id), UNIQUE INDEX portable_equipment_station (portable_equipment_id, station_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE station (id INT AUTO_INCREMENT NOT NULL, location VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE  `order`  ADD CONSTRAINT FK_E6AB879DB9D53E94 FOREIGN KEY (campervan_id) REFERENCES campervan (id)');
        $this->addSql('ALTER TABLE  `order`  ADD CONSTRAINT FK_E6AB879D53721DCB FOREIGN KEY (start_station_id) REFERENCES station (id)');
        $this->addSql('ALTER TABLE  `order`  ADD CONSTRAINT FK_E6AB879D2FF5EABB FOREIGN KEY (end_station_id) REFERENCES station (id)');
        $this->addSql('ALTER TABLE campervan ADD CONSTRAINT FK_6891BB7F21BDB235 FOREIGN KEY (station_id) REFERENCES station (id)');
        $this->addSql('ALTER TABLE ordered_equipment ADD CONSTRAINT FK_1308E95D1252C1E9 FOREIGN KEY (parent_order_id) REFERENCES  `order`  (id)');
        $this->addSql('ALTER TABLE ordered_equipment ADD CONSTRAINT FK_1308E95DB5AF2FC4 FOREIGN KEY (portable_equipment_id) REFERENCES portable_equipment (id)');
        $this->addSql('ALTER TABLE portable_equipment_station ADD CONSTRAINT FK_E66E257BB5AF2FC4 FOREIGN KEY (portable_equipment_id) REFERENCES portable_equipment (id)');
        $this->addSql('ALTER TABLE portable_equipment_station ADD CONSTRAINT FK_E66E257B21BDB235 FOREIGN KEY (station_id) REFERENCES station (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ordered_equipment DROP FOREIGN KEY FK_1308E95D1252C1E9');
        $this->addSql('ALTER TABLE  `order`  DROP FOREIGN KEY FK_E6AB879DB9D53E94');
        $this->addSql('ALTER TABLE ordered_equipment DROP FOREIGN KEY FK_1308E95DB5AF2FC4');
        $this->addSql('ALTER TABLE portable_equipment_station DROP FOREIGN KEY FK_E66E257BB5AF2FC4');
        $this->addSql('ALTER TABLE  `order`  DROP FOREIGN KEY FK_E6AB879D53721DCB');
        $this->addSql('ALTER TABLE  `order`  DROP FOREIGN KEY FK_E6AB879D2FF5EABB');
        $this->addSql('ALTER TABLE campervan DROP FOREIGN KEY FK_6891BB7F21BDB235');
        $this->addSql('ALTER TABLE portable_equipment_station DROP FOREIGN KEY FK_E66E257B21BDB235');
        $this->addSql('DROP TABLE  `order` ');
        $this->addSql('DROP TABLE campervan');
        $this->addSql('DROP TABLE ordered_equipment');
        $this->addSql('DROP TABLE portable_equipment');
        $this->addSql('DROP TABLE portable_equipment_station');
        $this->addSql('DROP TABLE station');
    }
}
