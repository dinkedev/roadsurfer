<?php

namespace App\Repository;

use App\Entity\PortableEquipmentStation;
use App\Entity\Station;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PortableEquipmentStationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PortableEquipmentStation::class);
    }

    /**
     * @param Station $station
     * @return Station[]
     */
    public function findByStation(Station $station)
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.portableEquipment', 'pe')
            ->addSelect('pe')
            ->andWhere('p.station = :station')
            ->setParameter('station', $station)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return PortableEquipmentStation[]
     */
    public function findAllPortableEquipmentData()
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.portableEquipment', 'portableEquipment')
            ->addSelect('portableEquipment')
            ->innerJoin('p.station', 'station')
            ->addSelect('station')
            ->getQuery()
            ->getResult();
    }
}
