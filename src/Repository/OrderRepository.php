<?php

namespace App\Repository;

use App\Entity\Order;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * Get lastOrder endDate or null when no entries
     *
     * @return int|null
     */
    public function findMaximumOrderEndDate()
    {
        return $this->createQueryBuilder('o')
            ->select('MAX(o.endDate)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Find all orders where start or end date matches to passed date
     *
     * @param DateTimeInterface $date
     * @return Order[]
     */
    public function findAllOrdersByDate(DateTimeInterface $date)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.startDate = :date or o.endDate = :date')
            ->setParameter('date', $date->format('Y-m-d'))
            ->orderBy('o.startDate', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
