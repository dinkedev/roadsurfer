<?php

namespace App\Command;

use App\Service\BookingTimelineService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateTimelineCommand extends Command
{
    protected static $defaultName = 'app:create-timeline';
    protected static $defaultDescription = 'Output timeline entries';
    private BookingTimelineService $timelineService;

    public function __construct(BookingTimelineService $timelineService)
    {
        $this->timelineService = $timelineService;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $timeline = $this->timelineService->getBookingTimeline();

        if (!$timeline) {
            $io->error('Empty timeline. Have you setup Fixtures?');
        }

        $this->outputData($timeline, $io);

        $io->success('Done processing');

        return Command::SUCCESS;
    }

    /**
     * Output data
     *
     * @param array $timeline
     * @param SymfonyStyle $io
     */
    private function outputData(array $timeline, SymfonyStyle $io): void
    {
        foreach ($timeline as $date => $data) {
            $io->newLine(1);
            $io->writeln('Date: ' . $date);

            foreach ($data as $station => $equipments) {
                $io->writeln('------------------');
                $io->writeln('Station: ' . $station);
                $io->writeln('------------------');

                foreach ($equipments as $equipment) {
                    $io->writeln($equipment['name']);
                    $io->writeln(' - Available: ' . $equipment['available']);
                    $io->writeln(' - Booked: ' . $equipment['booked']);
                }
            }
        }
    }
}
