<?php

namespace App\Entity;

use App\Repository\TimelineRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TimelineRepository::class)
 */
class Timeline
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $station;

    /**
     * @ORM\ManyToOne(targetEntity=PortableEquipment::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $portableEquipment;

    /**
     * @ORM\Column(type="integer")
     */
    private $booked;

    /**
     * @ORM\Column(type="integer")
     */
    private $available;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getPortableEquipment(): ?PortableEquipment
    {
        return $this->portableEquipment;
    }

    public function setPortableEquipment(?PortableEquipment $portableEquipment): self
    {
        $this->portableEquipment = $portableEquipment;

        return $this;
    }

    public function getBooked(): ?int
    {
        return $this->booked;
    }

    public function setBooked(int $booked): self
    {
        $this->booked = $booked;

        return $this;
    }

    public function getAvailable(): ?int
    {
        return $this->available;
    }

    public function setAvailable(int $available): self
    {
        $this->available = $available;

        return $this;
    }
}
