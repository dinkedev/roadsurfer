<?php

namespace App\Entity;

use App\Repository\OrderedEquipmentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderedEquipmentRepository::class)
 */
class OrderedEquipment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderedEquipment")
     * @ORM\JoinColumn(nullable=false)
     */
    private $parentOrder;

    /**
     * @ORM\ManyToOne(targetEntity=PortableEquipment::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $portableEquipment;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParentOrder(): ?Order
    {
        return $this->parentOrder;
    }

    public function setParentOrder(?Order $parentOrder): self
    {
        $this->parentOrder = $parentOrder;

        return $this;
    }

    public function getPortableEquipment(): ?PortableEquipment
    {
        return $this->portableEquipment;
    }

    public function setPortableEquipment(?PortableEquipment $portableEquipment): self
    {
        $this->portableEquipment = $portableEquipment;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
