<?php

namespace App\Entity;

use App\Repository\StationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StationRepository::class)
 */
class Station
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="startStation")
     */
    private $startStationOrders;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="endStation")
     */
    private $endStationOrders;

    /**
     * @ORM\OneToMany(targetEntity=Campervan::class, mappedBy="station")
     */
    private $campervans;

    /**
     * @ORM\OneToMany(targetEntity=PortableEquipmentStation::class, mappedBy="station", orphanRemoval=true)
     */
    private $portableEquipmentStations;

    public function __construct()
    {
        $this->startStationOrders = new ArrayCollection();
        $this->endStationOrders = new ArrayCollection();
        $this->campervans = new ArrayCollection();
        $this->portableEquipmentStations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getStartStationOrders(): Collection
    {
        return $this->startStationOrders;
    }

    /**
     * @return Collection|Order[]
     */
    public function getEndStationOrders(): Collection
    {
        return $this->endStationOrders;
    }

    /**
     * @return Collection|Campervan[]
     */
    public function getCampervans(): Collection
    {
        return $this->campervans;
    }

    public function addCampervan(Campervan $campervan): self
    {
        if (!$this->campervans->contains($campervan)) {
            $this->campervans[] = $campervan;
            $campervan->setStation($this);
        }

        return $this;
    }

    public function removeCampervan(Campervan $campervan): self
    {
        if ($this->campervans->removeElement($campervan)) {
            // set the owning side to null (unless already changed)
            if ($campervan->getStation() === $this) {
                $campervan->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PortableEquipmentStation[]
     */
    public function getPortableEquipmentStations(): Collection
    {
        return $this->portableEquipmentStations;
    }

    public function addPortableEquipmentStation(PortableEquipmentStation $portableEquipmentStation): self
    {
        if (!$this->portableEquipmentStations->contains($portableEquipmentStation)) {
            $this->portableEquipmentStations[] = $portableEquipmentStation;
            $portableEquipmentStation->setStation($this);
        }

        return $this;
    }

    public function removePortableEquipmentStation(PortableEquipmentStation $portableEquipmentStation): self
    {
        if ($this->portableEquipmentStations->removeElement($portableEquipmentStation)) {
            // set the owning side to null (unless already changed)
            if ($portableEquipmentStation->getStation() === $this) {
                $portableEquipmentStation->setStation(null);
            }
        }

        return $this;
    }
}
