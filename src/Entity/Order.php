<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Table name under backticks because of reserved keyword (order)
 *
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name=" `order` ")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Campervan::class, inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $campervan;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="startStationOrders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $startStation;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="endStationOrders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $endStation;

    /**
     * @ORM\Column(type="date")
     */
    private $startDate;

    /**
     * @ORM\Column(type="date")
     */
    private $endDate;

    /**
     * @ORM\OneToMany(targetEntity=OrderedEquipment::class, mappedBy="parentOrder")
     */
    private $orderedEquipment;

    public function __construct()
    {
        $this->orderedEquipment = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCampervan(): ?Campervan
    {
        return $this->campervan;
    }

    public function setCampervan(?Campervan $campervan): self
    {
        $this->campervan = $campervan;

        return $this;
    }

    public function getStartStation(): ?Station
    {
        return $this->startStation;
    }

    public function setStartStation(?Station $startStation): self
    {
        $this->startStation = $startStation;

        return $this;
    }

    public function getStartDate(): ?DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return Collection|OrderedEquipment[]
     */
    public function getOrderedEquipment(): Collection
    {
        return $this->orderedEquipment;
    }

    public function addOrderedEquipment(OrderedEquipment $orderedEquipment): self
    {
        if (!$this->orderedEquipment->contains($orderedEquipment)) {
            $this->orderedEquipment[] = $orderedEquipment;
            $orderedEquipment->setParentOrder($this);
        }

        return $this;
    }

    public function removeOrderedEquipment(OrderedEquipment $orderedEquipment): self
    {
        if ($this->orderedEquipment->removeElement($orderedEquipment)) {
            // set the owning side to null (unless already changed)
            if ($orderedEquipment->getParentOrder() === $this) {
                $orderedEquipment->setParentOrder(null);
            }
        }

        return $this;
    }

    public function getEndStation(): ?Station
    {
        return $this->endStation;
    }

    public function setEndStation(?Station $endStation): self
    {
        $this->endStation = $endStation;

        return $this;
    }
}
