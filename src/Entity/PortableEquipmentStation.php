<?php

namespace App\Entity;

use App\Repository\PortableEquipmentStationRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass=PortableEquipmentStationRepository::class)
 *
 * @Table(name="portable_equipment_station",
 *    uniqueConstraints={
 *        @UniqueConstraint(name="portable_equipment_station",
 *            columns={"portable_equipment_id", "station_id"})
 *    }
 * )
 */
class PortableEquipmentStation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=PortableEquipment::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $portableEquipment;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="portableEquipmentStations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $station;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPortableEquipment(): ?PortableEquipment
    {
        return $this->portableEquipment;
    }

    public function setPortableEquipment(?PortableEquipment $portableEquipment): self
    {
        $this->portableEquipment = $portableEquipment;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
