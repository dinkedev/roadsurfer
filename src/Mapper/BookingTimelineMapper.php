<?php

namespace App\Mapper;

use JsonSerializable;

/**
 * Class BookingTimelineMapper
 *
 * Remapping timeline booking data to proper API format
 */
class BookingTimelineMapper implements JsonSerializable
{
    private array $timelineData;

    public function __construct(array $timelineData)
    {
        $this->timelineData = $timelineData;
    }

    public static function createFromArray($timelineData): self
    {
        return new self($timelineData);
    }

    /**
     * Remap data to proper format which can be consumed by clients
     *
     * @return array|array[]
     */
    public function jsonSerialize()
    {
        $jsonData = [];

        if (empty ($this->timelineData)) {
            return $jsonData;
        }

        foreach ($this->timelineData as $date => $timelineData) {
            $current = ['date' => $date];
            foreach ($timelineData as $station => $equipments) {
                $stationData['location'] = $station;

                $portableEquipments = [];
                foreach ($equipments as $equipment) {
                    $potableEquipment['name'] = $equipment['name'];
                    $potableEquipment['available'] = $equipment['available'];
                    $potableEquipment['booked'] = $equipment['booked'];

                    $portableEquipments[] = $potableEquipment;
                }

                $stationData['portableEquipments'] = $portableEquipments;
                $current['stations'][] = $stationData;
            }

            $jsonData[] = $current;
        }

        return ['timeline' => $jsonData];
    }
}