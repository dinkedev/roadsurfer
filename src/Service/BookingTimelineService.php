<?php

namespace App\Service;

use App\Repository\OrderRepository;
use App\Repository\PortableEquipmentStationRepository;
use DateInterval;
use DatePeriod;
use DateTime;
use Exception;

class BookingTimelineService
{
    public const DATE_FORMAT = 'Y-m-d';

    private PortableEquipmentStationRepository $portableEquipmentStationRepository;
    private OrderRepository $orderRepository;

    private array $equipmentData = [];

    public function __construct(
        PortableEquipmentStationRepository $portableEquipmentStationRepository,
        OrderRepository $orderRepository
    ) {
        $this->portableEquipmentStationRepository = $portableEquipmentStationRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Process all portable Equipment data and return them as array map
     * by taking data from current date until last return date from Orders
     *
     * @todo pass from/to dates as parameters
     *
     * @return array
     */
    public function getBookingTimeline(): array
    {
        //store actual portableEquipmentData to class property array
        $this->equipmentData = $this->getActualEquipmentData();

        //Generate range array from current date till last order return
        $lastOrderReturnDate = $this->orderRepository->findMaximumOrderEndDate();
        if ($lastOrderReturnDate === null) {
            return $this->equipmentData;
        }

        $futureDateRange = $this->getDateRange($lastOrderReturnDate);

        foreach ($futureDateRange as $date) {
            $currentDateIndex = $date->format(self::DATE_FORMAT);
            $previousDateIndex = (clone $date)->modify("-1 days")->format(self::DATE_FORMAT);

            // set initial data for date from previous date (if doesn't exists)
            if (!array_key_exists($currentDateIndex, $this->equipmentData)) {
                $this->equipmentData[$currentDateIndex] = $this->equipmentData[$previousDateIndex];
            }

            $orders = $this->orderRepository->findAllOrdersByDate($date);

            foreach ($orders as $order) {
                $orderedEquipment = $order->getOrderedEquipment();

                foreach ($orderedEquipment as $equipment) {
                    $equipmentId = $equipment->getPortableEquipment()->getId();
                    if ($order->getStartDate()->format(self::DATE_FORMAT) === $date->format(self::DATE_FORMAT)) {
                        $startStationName = $order->getStartStation()->getLocation();
                        $this->equipmentData[$currentDateIndex][$startStationName][$equipmentId]['available']
                            -= $equipment->getQuantity();
                        $this->equipmentData[$currentDateIndex][$startStationName][$equipmentId]['booked']
                           += $equipment->getQuantity();
                    } elseif ($order->getEndDate()->format(self::DATE_FORMAT) === $date->format(self::DATE_FORMAT)) {
                        $startStationName = $order->getStartStation()->getLocation();
                        $endStationName = $order->getEndStation()->getLocation();
                        $this->equipmentData[$currentDateIndex][$endStationName][$equipmentId]['available']
                            += $equipment->getQuantity();
                        $this->equipmentData[$currentDateIndex][$startStationName][$equipmentId]['booked']
                            -= $equipment->getQuantity();
                    }
                }
            }
        }

        return $this->equipmentData;
    }

    /**
     * Get DatePeriod from current date till lastDate
     *
     * @param string $lastDate
     * @return DatePeriod
     * @throws Exception
     */
    public function getDateRange(string $lastDate): DatePeriod
    {
        return new DatePeriod(
            new DateTime(),
            new DateInterval('P1D'),
            (new DateTime($lastDate))->modify('+1 day')
        );
    }

    /**
     * Get actual Equipment data from DB
     * and format it as array map
     *
     * @return array
     */
    public function getActualEquipmentData(): array
    {
        $currentDate = (new DateTime())->format(self::DATE_FORMAT);

        $portableEquipmentStations = $this->portableEquipmentStationRepository->findAllPortableEquipmentData();

        $equipmentData = [];
        foreach ($portableEquipmentStations as $portableEquipmentStation) {
            $stationName = $portableEquipmentStation->getStation()->getLocation();
            $id = $portableEquipmentStation->getPortableEquipment()->getId();
            $available = $portableEquipmentStation->getQuantity();
            $name = $portableEquipmentStation->getPortableEquipment()->getName();

            $equipmentData[$currentDate][$stationName][$id] = [
                'name' => $name,
                'available' => $available,
                'booked' => 0
            ];
        }

        return $equipmentData;
    }
}