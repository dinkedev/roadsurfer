<?php

namespace App\DataFixtures;

use App\Entity\Campervan;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CampervanFixtures extends Fixture implements DependentFixtureInterface
{
    public const CAMPERVANS = [
        'Mercedes Marco Polo Horizon',
        'VW T6 California Ocean',
        'VW T6 California Beach',
        'VW T6.1 California Ocean',
        'Mercedes Marco Polo',
        'Ford Nugget mit Aufstelldach',
        'Westfalia Columbus',
        'Bürstner Eliseo',
        'VW T6 Transporter',
        'Ford Nugget Plus mit Aufstelldach'
    ];

    public function getDependencies()
    {
        return [
            StationFixtures::class,
        ];
    }

    public function load(ObjectManager $manager)
    {
        foreach (self::CAMPERVANS as $key => $model) {
            $campervan = (new Campervan())
                ->setModel($model);

            if ($key % 2 === 0) {
                $campervan->setStation($this->getReference(StationFixtures::MUNICH_STATION_REFERENCE));
            } else {
                $campervan->setStation($this->getReference(StationFixtures::BERLIN_STATION_REFERENCE));
            }

            $manager->persist($campervan);
            $this->addReference('campervan-' . $key, $campervan);
        }

        $manager->flush();
    }
}
