<?php

namespace App\DataFixtures;

use App\Entity\PortableEquipment;
use App\Entity\PortableEquipmentStation;
use App\Entity\Station;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PortableEquipmentFixtures extends Fixture
{
    const PORTABLE_EQUIPMENTS = [
        'toilet',
        'shower',
        'blankets',
        'tables',
        'chairs',
        'coffee-pots',
        'kettle',
        'bet sheets',
    ];

    public function load(ObjectManager $manager)
    {
        /** @var Station $stationMunich */
        $stationMunich = $this->getReference(StationFixtures::MUNICH_STATION_REFERENCE);

        /** @var Station $stationBerlin */
        $stationBerlin = $this->getReference(StationFixtures::BERLIN_STATION_REFERENCE);

        foreach (self::PORTABLE_EQUIPMENTS as $portableEquipmentName) {
            $portableEquipment = (new PortableEquipment())
                ->setName($portableEquipmentName);

            $manager->persist($portableEquipment);
            $potableEquipmentStation =
                $this->createPortableEquipmentStation($portableEquipment, $stationMunich, rand(10,20));
            $manager->persist($potableEquipmentStation);
            $this->addReference('equipment-' . $portableEquipmentName, $portableEquipment);
            $potableEquipmentStation =
                $this->createPortableEquipmentStation($portableEquipment, $stationBerlin, rand(10, 20));
            $manager->persist($potableEquipmentStation);
        }

        $manager->flush();
    }

    private function createPortableEquipmentStation(
        PortableEquipment $portableEquipment,
        Station $station,
        int $quantity
    ): PortableEquipmentStation {
        $portableEquipmentStation = (new PortableEquipmentStation())
            ->setPortableEquipment($portableEquipment)
            ->setStation($station)
            ->setQuantity($quantity);

        return $portableEquipmentStation;
    }
}
