<?php

namespace App\DataFixtures;

use App\Entity\Station;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class StationFixtures extends Fixture
{
    const STATIONS = [
        'Paris',
        'London',
        'Munich',
        'Berlin',
        'Hamburg',
        'Belgrade',
        'Barcelona',
        'Budapest',
        'Prague'
    ];

    // reference to two used for easy access
    public const MUNICH_STATION_REFERENCE = 'station-munich';
    public const BERLIN_STATION_REFERENCE = 'station-berlin';

    public function load(ObjectManager $manager)
    {
        foreach (self::STATIONS as $location) {
            $station = (new Station())
                ->setLocation($location);

            $manager->persist($station);

            $this->addReference('station-' . strtolower($location), $station);
        }

        $manager->flush();
    }
}
