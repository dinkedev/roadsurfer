<?php

namespace App\DataFixtures;

use App\Entity\Campervan;
use App\Entity\Order;
use App\Entity\OrderedEquipment;
use App\Entity\PortableEquipment;
use App\Entity\Station;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class OrderFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return [
            StationFixtures::class,
            PortableEquipmentFixtures::class,
        ];
    }

    public function load(ObjectManager $manager)
    {
        //create two orders
        // 1. From next day, duration 3 days (Munich->Munich)
        // 2. From one day after end of 1st order, duration 2 days (Munich -> Berlin)

        // Order 1 setup Munich -> Munich
        /** @var Campervan $campervan */
        $campervan = $this->getReference('campervan-0');

        // first start tomorrow and ends after 3 days
        $firsOrderStartDate = (new DateTime())->modify('+1 day');
        $firstOrderEndDate = (clone $firsOrderStartDate)->modify('+3 day');

        /** @var Station $stationMunich */
        $stationMunich = $this->getReference(StationFixtures::MUNICH_STATION_REFERENCE);
        $order = $this->makeOrder(
            $campervan,
            $stationMunich,
            $stationMunich,
            $firsOrderStartDate->format('Y-m-d'),
            $firstOrderEndDate->format('Y-m-d')
        );

        $portableEquipment = [
            $this->getReference('equipment-toilet'),
            $this->getReference('equipment-shower'),
            $this->getReference('equipment-coffee-pots')
        ];

        $manager->persist($order);
        $this->storeOrderedEquipment($manager, $portableEquipment, $order);


        // Order 2 setup Munich -> Berlin
        /** @var Campervan $campervan */
        $campervan = $this->getReference('campervan-1');

        $secondOrderStartDate = (clone $firstOrderEndDate)->modify('+1 day');
        $secondOrderEndDate = (clone $secondOrderStartDate)->modify('+2 day');

        /** @var Station $stationBerlin */
        $stationBerlin = $this->getReference(StationFixtures::BERLIN_STATION_REFERENCE);
        $order = $this->makeOrder(
            $campervan,
            $stationMunich,
            $stationBerlin,
            $secondOrderStartDate->format('Y-m-d'),
            $secondOrderEndDate->format('Y-m-d')
        );

        $portableEquipment = [
            $this->getReference('equipment-toilet'),
            $this->getReference('equipment-shower'),
            $this->getReference('equipment-blankets')
        ];

        $manager->persist($order);
        $this->storeOrderedEquipment($manager, $portableEquipment, $order);

        $manager->flush();
    }

    private function makeOrder(
        Campervan $campervan,
        Station $startStation,
        Station $endStation,
        string $startDate,
        string $endDate
    ): Order {
        $startDate = new DateTime($startDate);
        $endDate = new DateTime($endDate);
        $order = (new Order())
            ->setCampervan($campervan)
            ->setStartStation($startStation)
            ->setEndStation($endStation)
            ->setStartDate($startDate)
            ->setEndDate($endDate);

        return $order;
    }

    /**
     * @param PortableEquipment[] $portableEquipment
     * @param Order $order
     */
    private function storeOrderedEquipment(ObjectManager $manager, array $portableEquipment, Order $order)
    {
        foreach ($portableEquipment as $equipment) {
            $orderedEquipment = (new OrderedEquipment())
                ->setPortableEquipment($equipment)
                ->setQuantity(1)
                ->setParentOrder($order);
            $manager->persist($orderedEquipment);
        }
    }
}
