<?php

namespace App\Controller;

use App\Service\BookingTimelineService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TimelineController extends AbstractController
{
    private BookingTimelineService $bookingTimelineService;

    public function __construct (BookingTimelineService $bookingTimelineService)
    {
        $this->bookingTimelineService = $bookingTimelineService;
    }

    /**
     * @Route("/timeline", name="timeline")
     */
    public function index(): Response
    {
        $timeline = $this->bookingTimelineService->getBookingTimeline();

        return $this->render('timeline/index.html.twig', [
            'timeline' => $timeline,
        ]);
    }
}
