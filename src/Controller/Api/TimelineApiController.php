<?php

namespace App\Controller\Api;

use App\Mapper\BookingTimelineMapper;
use App\Service\BookingTimelineService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TimelineApiController extends AbstractController
{
    private BookingTimelineService $bookingTimelineService;

    public function __construct (BookingTimelineService $bookingTimelineService)
    {
        $this->bookingTimelineService = $bookingTimelineService;
    }

    /**
     * @Route("/api/timeline", name="api_timeline")
     */
    public function index(): Response
    {
        $timeline = $this->bookingTimelineService->getBookingTimeline();

        return $this->json(BookingTimelineMapper::createFromArray($timeline));
    }
}
