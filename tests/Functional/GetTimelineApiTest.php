<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GetTimelineApiTest extends WebTestCase
{
    public function testTimelineApiReturnsExpectedFormat(): void
    {
        $client = static::createClient();
        $client->request('GET', '/api/timeline');
        $this->assertResponseIsSuccessful();
        $this->testApiResponseIsInCorrectFormat($client->getResponse()->getContent());
    }

    private function testApiResponseIsInCorrectFormat(string $response)
    {
        $data = json_decode($response, true);
        self::assertArrayHasKey('timeline', $data);
        self::assertArrayHasKey('date', $data['timeline'][0]);
        self::assertArrayHasKey('stations', $data['timeline'][0]);
        self::assertArrayHasKey('location', $data['timeline'][0]['stations'][0]);
        self::assertArrayHasKey('portableEquipments', $data['timeline'][0]['stations'][0]);
    }
}
