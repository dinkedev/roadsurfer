<?php

namespace App\Tests\Repository;

use App\Entity\PortableEquipmentStation;
use App\Repository\PortableEquipmentStationRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PortableEquipmentStationRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testFindAllPortableEquipmentData(): void
    {
        /** @var PortableEquipmentStationRepository $orderRepository */
        $portableEquipmentStationRepository = $this->entityManager
            ->getRepository(PortableEquipmentStation::class);

        $allData = $portableEquipmentStationRepository->findAllPortableEquipmentData();

        self::assertEquals(16, count($allData));
        //@todo add more assertions
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}
