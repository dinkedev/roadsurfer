<?php

namespace App\Tests\Repository;

use App\Entity\Order;
use App\Repository\OrderRepository;
use App\Service\BookingTimelineService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class OrderRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testFindMaxReturnsProperDate(): void
    {
        /** @var OrderRepository $orderRepository */
        $orderRepository = $this->entityManager
            ->getRepository(Order::class);

        // in fixtures it's set so it's 7days from fixture load date
        $lastOrderDate = (new DateTime())->modify('+7 day')->format(BookingTimelineService::DATE_FORMAT);

        self::assertEquals($lastOrderDate, $orderRepository->findMaximumOrderEndDate());
    }

    public function testFindAllOrdersByDateReturnsExpectedOrder(): void
    {
        /** @var OrderRepository $orderRepository */
        $orderRepository = $this->entityManager
            ->getRepository(Order::class);

        // in fixtures 1st order is set to be 1 day after fixture load date
        // which is exactly what we check in expectation here
        $firstOrderDate = (new DateTime())->modify('+1 day');
        $orders = $orderRepository->findAllOrdersByDate($firstOrderDate);

        self::assertEquals(1, count($orders));
        //@todo more assertions
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}
