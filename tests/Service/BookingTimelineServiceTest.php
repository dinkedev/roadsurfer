<?php

namespace App\Tests\Service;

use App\Repository\OrderRepository;
use App\Service\BookingTimelineService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BookingTimelineServiceTest extends KernelTestCase
{
    /**
     * @var BookingTimelineService
     */
    private $bookingTimelineService;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function setUp(): void
    {
        // (1) boot the Symfony kernel
        self::bootKernel();

        // (2) use static::getContainer() to access the service container
        $container = static::getContainer();

        $this->bookingTimelineService = $container->get(BookingTimelineService::class);
        $this->orderRepository = $container->get(OrderRepository::class);

        parent::setUp();
    }

    public function testCurrentDateMatchesToTimelineCurrentDate(): void
    {
        //get current date
        $currentDate = (new DateTime())->format(BookingTimelineService::DATE_FORMAT);

        $currentPortableEquipmentData = $this->bookingTimelineService->getActualEquipmentData();
        $timeline = $this->bookingTimelineService->getBookingTimeline();

        // make sure that entries are matching
        self::assertEquals($currentPortableEquipmentData[$currentDate], $timeline[$currentDate]);
    }

    /**
     * Properly test actual functionality
     *
     * @todo use repositories to get actual data from DB
     */
    public function testTimelineCorrectlyCalculateAvailableItems(): void
    {
        $timeline = $this->bookingTimelineService->getBookingTimeline();
        $actualData = $this->bookingTimelineService->getActualEquipmentData();

        //assert that current date is the same as timeline start
        $currentDate = (new DateTime())->format(BookingTimelineService::DATE_FORMAT);
        self::assertEquals($actualData[$currentDate], $timeline[$currentDate]);

        foreach ($timeline as $date => $timelineData) {
            $orders = $this->orderRepository->findAllOrdersByDate((new DateTime($date)));
            //@todo finish the test
            //make sure proper values are increased/decreased on return dates
        }
    }
}