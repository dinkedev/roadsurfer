# roadsurfer Coding Challenge

Booking Timeline for "roadsurfer like" campervan booking company.

## Requirements

- PHP 7.4+
- Database (Tested with MySQL 8.x, in theory should be working with any Doctrine supported DB)

## Installation

- clone repo from bitbucket 
  ```git clone https://user@bitbucket.org/dinkedev/roadsurfer.git```
- copy .env file to .env.local and setup proper DATABASE_URL
- migrate DB by running: ```php bin/console doctrine:migrations:migrate```
- load DB fixtures: ```php bin/console doctrine:fixtures:load```
- Start symfony server with ```symfony server:start``` or setup web server

## Initial Notes
The code has been made by using currently the latest stable version of Symfony (5.3.3). I've created fixtures which should fill in DB with some initial data and help testing. You can of course edit or add more data by using DB client (unfortunately I didn't have enough time to create CRUD endpoints for that).

Current fixtures will create data which will be related to two stations (Munich and Berlin), along with some portable equipments with random quantities on respective stations. 

Order fixtures will create exactly two orders:
1. Munich -> Munich which will always start 1 day after fixture load date and lasts 3 days.
2. Munich -> Berlin which will always start 1 day after 1st order ends and lasts 2 days.

## DB Schema
![Scheme](docs/roadsurfer.png)

## Code Description
Majority of timeline calculation complexity lives in BookingTimelineService, which basically creates array map with current portable equipment data in memory, traverse through all orders, apply changes to respective array elements and returns it. This processing is expensive operation and ideally it should be run by cron and results should be stored into DB, instead of calculating data per each request (actual timeline table is already created).

Please note that BookingTimelineService will always return timeline from present day, until last returned date in orders table! In real life scenario, we would probably want to limit that range with from/to values.

There are 3 ways to review the generated timeline:

- Using symfony command: ```php bin/console app:create-timeline ```
- Using /timeline url from browser (which will display timeline data in html table)
- Using GET /api/timeline endpoint, which will display the data in proper JSON format (see bellow).

```
{
    "timeline": [
        {
            "date": "2021-07-18",
            "stations": [
                {
                    "location": "Munich",
                    "portableEquipments": [
                        {
                            "name": "toilet",
                            "available": 20,
                            "booked": 0
                        },
                        ... 
                    ]
                },
                {
                    "location": "Berlin",
                    "portableEquipments": [
                        {
                            "name": "toilet",
                            "available": 19,
                            "booked": 0
                        },
                        ...
                    ]
                },
                ...
            ]
        },
        ...
    ]
}
```

## Tests

In order to properly run functional tests, proper test DB should be configured:
```
php bin/console --env=test doctrine:database:create
php bin/console --env=test doctrine:schema:create
php bin/console doctrine:fixtures:load --env=test
```

You can run tests with:

```
vendor/bin/phpunit
PHPUnit 9.5.6 by Sebastian Bergmann and contributors.

Testing
.....                                                               5 / 5 (100%)

Time: 00:00.336, Memory: 22.00 MB

OK (5 tests, 10 assertions)

```

## Final Notes


I did my best to come up with the best possible solution under given timeframe. The coding style I used is based on PSR-12. Personally I rarely use PHPDocs since introduction of proper TypeHints in PHP, but here I used them sometimes, where I found appropriate.

